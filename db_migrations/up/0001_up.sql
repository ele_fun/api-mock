Create Database If Not Exists BlaDB;

-- Crear tabla
CREATE TABLE BlaDB.endpoints (
    id INT AUTO_INCREMENT PRIMARY KEY,
    path TEXT NOT NULL COMMENT '',
    method CHAR(10) NOT NULL COMMENT '',
    output JSON COMMENT '',
    status INT COMMENT ''
);


Alter Table BlaDB.endpoints
    Add Index methodIDX (method),
    Add Index statusIDX (status)
;
