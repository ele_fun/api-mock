package config

import (
	"log"

	"github.com/ilyakaznacheev/cleanenv"
)

type (
	env struct {
		AppPort string `env:"app_port" env-default:"9009"`
		AppHost string `env:"app_host" env-default:"0.0.0.0"`

		DBMysqlDNS       string `env:"db_mysql_dns"`
		CacheRedisDomain string `env:"cache_redis_domain" env-default:"0.0.0.0:6379"`
		CacheRedisPass   string `env:"cache_redis_password"`
	}
)

var Env env = env{}

func InitEnv() error {
	err := cleanenv.ReadConfig(".env", &Env)
	if err != nil {
		log.Println("init environment, error: ", err)
	}
	return err
}
