package cacherepository

import (
	"context"

	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/models"
)

type (
	Endpoint interface {
		Get(ctx context.Context, endpoint models.Endpoint) (*models.Endpoint, error)
		Set(ctx context.Context, endpoint models.Endpoint) error
	}
)
