package redis

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	redisV9 "github.com/redis/go-redis/v9"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/config"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/models"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/pkg"
)

type (
	redis struct {
		rdb *redisV9.Client
	}
)

func NewRedis() (*redis, error) {
	rdb := redisV9.NewClient(
		&redisV9.Options{
			Addr:     config.Env.CacheRedisDomain,
			Password: config.Env.CacheRedisPass,
			DB:       0,
		},
	)
	return &redis{
		rdb: rdb,
	}, nil
}

func (r *redis) Get(ctx context.Context, endpoint models.Endpoint) (*models.Endpoint, error) {
	if endpoint.Path == nil || endpoint.Method == nil {
		return nil, fmt.Errorf("path o method nil")
	}
	key := fmt.Sprintf(pkg.KeysRedis.EndpointByPathAndMethod, *endpoint.Path, *endpoint.Method)

	value, err := r.rdb.Get(ctx, key).Result()
	if err == redisV9.Nil {
		log.Println("Cache, redis, not found: ", err)
		return nil, err
	}

	if err != nil {
		log.Println("Cache, redis, err: ", err)
		return nil, err
	}
	var end models.Endpoint
	err = json.Unmarshal([]byte(value), &end)
	if err != nil {
		log.Println("Cache, redis, unmarshall, err: ", err)
		return nil, err
	}

	return &end, nil
}
func (r *redis) Set(ctx context.Context, endpoint models.Endpoint) error {
	if endpoint.Path == nil || endpoint.Method == nil {
		log.Println("set cache, ednpoint: ", endpoint)
		return fmt.Errorf("path o method nil")
	}
	key := fmt.Sprintf(pkg.KeysRedis.EndpointByPathAndMethod, *endpoint.Path, *endpoint.Method)
	// convert data
	data, err := json.Marshal(endpoint)
	if err != nil {
		log.Println("Cache, redis, marshal, err: ", err)
		return err
	}
	err = r.rdb.Set(ctx, key, string(data), 2*time.Hour).Err()
	if err != nil {
		log.Println("Cache, redis, set, err: ", err)
		return err
	}

	return nil
}
