package cache

import (
	"log"

	cacherepository "gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/cache/cache_repository"
)

type (
	CacheStore struct {
		// TODO: repositories
		Endpoint cacherepository.Endpoint
	}
)

var Cache CacheStore

// Set -
func Set(c CacheStore) {
	log.Println("init cache")
	Cache = c
}
