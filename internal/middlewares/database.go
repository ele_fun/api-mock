package middlewares

import (
	"context"
	"log"
	"net/http"

	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/cache"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/database"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/models"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/pkg"
)

// ruta base, {host}/
func (m *middleware) Database(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// DONE: validar si el middleware del cache capturó la información
		// esto se realiza con la captura de la información del contexto
		log.Println("Dentro del middleware database, antes de validar el cache")
		var pipe models.Pipe
		if err := pkg.MasterPipe(r, pkg.Pipes.Data, &pipe); err == nil {
			if pipe.Found {
				next.ServeHTTP(w, r)
				return
			}
		}

		// TODO: captura de la ruta
		log.Println("Dentro del middleware database, despues de validar el cache")
		endpoint, err := database.DB.EndpointRepository.GetByPathAndMethod(r.Context(), models.Endpoint{
			Path:   pkg.Pointer[string](r.URL.RequestURI()),
			Method: &r.Method,
		})
		if err == nil {
			pipe.Endpoint, pipe.Found = *endpoint, true
			// FIXME: validar si el middleware del cache está activado
			cache.Cache.Endpoint.Set(r.Context(), *endpoint)
		}
		r = r.WithContext(context.WithValue(r.Context(), pkg.Pipes.Data, pipe))

		next.ServeHTTP(w, r)
	}
}
