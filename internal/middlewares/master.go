package middlewares

import (
	"log"
	"net/http"
)

// ruta base, {host}/master
func (m *middleware) Master(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Content-Type", "application/json")
		// TODO: captura de la ruta
		log.Println("Dentro del middleware master")
		next.ServeHTTP(w, r)
	}
}
