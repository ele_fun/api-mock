package middlewares

import (
	"context"
	"log"
	"net/http"

	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/cache"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/models"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/pkg"
)

// ruta base, {host}/
func (m *middleware) Cache(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// TODO: captura de la ruta
		var pipe models.Pipe
		pipe.Endpoint.Path, pipe.Endpoint.Method = pkg.Pointer[string](r.URL.RequestURI()), &r.Method
		log.Println("middleware cache, path:", *pipe.Endpoint.Path, " - method: ", *pipe.Endpoint.Method)

		endpoint, err := cache.Cache.Endpoint.Get(r.Context(), pipe.Endpoint)
		if err == nil {
			pipe.Endpoint, pipe.Found = *endpoint, true
		}
		//ctxValue := context.WithValue(r.Context(), pkg.Pipes.Cache, pipe)
		ctxValue := context.WithValue(r.Context(), pkg.Pipes.Data, pipe)
		r = r.WithContext(ctxValue)

		next.ServeHTTP(w, r)
	}
}
