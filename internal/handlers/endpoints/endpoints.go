package endpointshandler

import (
	"encoding/json"
	"io"
	"log"
	"net/http"

	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/database"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/models"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/pkg"
)

type (
	handler struct{}
)

var Handler handler = handler{}

// New - creación de un endpoint, para su posterior consulta
func (h *handler) New(w http.ResponseWriter, r *http.Request) {
	// DONE: validacion del input
	bodySlice, err := io.ReadAll(r.Body)
	if err != nil {
		// DONE: pensarlo después
		w.WriteHeader(http.StatusConflict)
		json.NewEncoder(w).Encode(err)
		log.Println("io.ReadAll, error: ", err)
		return
	}
	var endPointNew models.EndpontNewInput
	err = json.Unmarshal(bodySlice, &endPointNew)
	if err != nil {
		w.WriteHeader(http.StatusConflict)
		json.NewEncoder(w).Encode(err)
		log.Println("json.Unmarshal, error: ", err)
		return
	}
	// DONE: validacion de duplicado
	duplicate, err := database.DB.EndpointRepository.GetByPathAndMethod(r.Context(), models.Endpoint{
		Path:   endPointNew.Path,
		Method: endPointNew.Method,
	})
	if duplicate != nil {
		w.WriteHeader(http.StatusConflict)
		json.NewEncoder(w).Encode(struct {
			Message string `json:"message"`
		}{Message: "endpoint already exist"})
		return
	}

	bodyOutputSlice, err := json.Marshal(*endPointNew.Output)
	if err != nil {
		// DONE: pensarlo
		bodyOutputSlice = []byte{}
	}

	// DONE: insert db
	result, err := database.DB.EndpointRepository.New(r.Context(), models.Endpoint{
		Path:   endPointNew.Path,
		Method: endPointNew.Method,
		Output: pkg.Pointer[string](string(bodyOutputSlice)),
		Status: endPointNew.Status,
	})
	if err != nil {
		// DONE: pensarlo después
		w.WriteHeader(http.StatusConflict)
		json.NewEncoder(w).Encode(err)
		return
	}

	// DONE: return
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(result)
}
