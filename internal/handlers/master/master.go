package master

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/models"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/pkg"
)

type (
	handler struct{}
)

var Handler handler = handler{}

type (
	responseHttp struct {
		Success bool             `json:"success"`
		Message string           `json:"message"`
		Data    *models.Endpoint `json:"data,omitempty"`
		Error   error            `json:"error,omitempty"`
	}
)

func (h *handler) Master(w http.ResponseWriter, r *http.Request) {
	var response responseHttp
	// obtener la información del contexto DATA
	var data models.Pipe
	err := pkg.MasterPipe(r, pkg.Pipes.Data, &data)
	response.Data = &data.Endpoint
	if err != nil || !data.Found {
		response.Message = "error pipe data"
		response.Error = fmt.Errorf("error: %v", err)
		response.Data = nil
	}

	if response.Data != nil {
		response.Success = true
		var endpointOutput models.EndpointOutput
		err = pkg.ConvertStringToStruct(*response.Data.Output, &endpointOutput)
		if err != nil {
			w.WriteHeader(http.StatusConflict)
			json.NewEncoder(w).Encode(struct {
				Message string `json:"message"`
			}{Message: fmt.Sprint("err convertStringToStruct: ", err)})
			return
		}

		w = setHeaders(w, endpointOutput.Headers)
		code := setCode(endpointOutput.Code)
		w.WriteHeader(int(code))

		json.NewEncoder(w).Encode(endpointOutput.Body)
		return
	}
	// TODO: response json
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func setHeaders(w http.ResponseWriter, headers []models.EndpointOutputHeader) http.ResponseWriter {
	if headers != nil && len(headers) > 0 {
		for _, h := range headers {
			if h.Key != nil && h.Value != nil {
				w.Header().Set(*h.Key, *h.Value)
			}
		}
	}
	return w
}
func setCode(c *uint16) uint16 {
	var code uint16 = http.StatusOK
	if c != nil {
		code = *c
	}
	return code
}
