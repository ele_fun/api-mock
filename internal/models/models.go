package models

type (
	Midd struct {
		Domain string `json:"domain"`
		Method string `json:"method"`
		Url    string `json:"url"`
		Data   any    `json:"data,omitempty"`
	}
	Pipe struct {
		Endpoint Endpoint
		Found    bool
	}
)

type (
	Endpoint struct {
		Path   *string `json:"path"`
		Method *string `json:"method"`
		Output *string `json:"output"`
		Status *uint8  `json:"status"`
	}
	Pagging struct {
		Size int
		Page int
	}
)
type (
	EndpontNewInput struct {
		Path   *string         `json:"path"`
		Method *string         `json:"method"`
		Output *EndpointOutput `json:"output"`
		Status *uint8          `json:"status"`
	}
)
type (
	EndpointOutput struct {
		Headers []EndpointOutputHeader `json:"headers,omitempty"`
		Body    any                    `json:"body"`
		Code    *uint16                `json:"code"`
	}
	EndpointOutputHeader struct {
		Key   *string `json:"key"`
		Value *string `json:"value"`
	}
)
