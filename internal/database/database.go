package database

import (
	"log"

	dbrepository "gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/database/db_repository"
)

type (
	Database struct {
		// TODO: repositories
		EndpointRepository dbrepository.EndpointRepository
	}
)

var DB Database

// Set -
func Set(db Database) {
	log.Println("init database")
	DB = db
}
