package dbrepository

import (
	"context"

	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/models"
)

type (
	EndpointRepository interface {
		All(ctx context.Context, paggin *models.Pagging) ([]models.Endpoint, error)
		GetByPathAndMethod(ctx context.Context, in models.Endpoint) (*models.Endpoint, error)
		New(ctx context.Context, in models.Endpoint) (*models.Endpoint, error)
	}
)
