package mysql

import (
	"context"
	"fmt"
	"log"
	"strings"

	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/models"
)

func (m *mysql) All(ctx context.Context, paggin *models.Pagging) ([]models.Endpoint, error) {
	out := []models.Endpoint{}
	query := fmt.Sprintf("Select path, method, output, status From BlaDB.endpoints ")
	// validation pagging
	if paggin != nil {
		offSet := (paggin.Page - 1) * paggin.Size
		query = fmt.Sprintf("%s Limit %d Offset %d", query, paggin.Size, offSet)
	}

	rows, err := m.db.QueryContext(ctx, query)
	if err != nil {
		log.Println("mysql, queryContext, all, error: ", err)
		return out, err
	}
	defer rows.Close()

	if err := rows.Err(); err != nil {
		log.Println("mysql, all, error: ", err)
		return out, err
	}
	for rows.Next() {
		var item models.Endpoint
		err := rows.Scan(&item.Path, &item.Method, &item.Output, &item.Status)
		if err != nil {
			log.Println("mysql, all, for, error: ", err)
			return []models.Endpoint{}, err
		}
		out = append(out, item)
	}

	return out, nil
}
func (m *mysql) GetByPathAndMethod(ctx context.Context, in models.Endpoint) (*models.Endpoint, error) {
	if in.Method == nil || in.Path == nil {
		return nil, fmt.Errorf("method or path empty")
	}
	// trim
	*in.Method = strings.Trim(*in.Method, "")
	*in.Path = strings.Trim(*in.Path, "")

	query := fmt.Sprintf("Select path, method, output, status From BlaDB.endpoints Where path = ? And method = ? Limit 1")
	result := m.db.QueryRowContext(ctx, query, in.Path, in.Method)
	err := result.Err()
	if err != nil {
		log.Println("mysql, GetByPathAndMethod, error: ", err)
		return nil, err
	}
	var endpoint models.Endpoint
	err = result.Scan(&endpoint.Path, &endpoint.Method, &endpoint.Output, &endpoint.Status)
	if err != nil {
		log.Println("mysql, GetByPathAndMethod, scan, error: ", err)
		return nil, err
	}
	return &endpoint, nil
}

func (m *mysql) New(ctx context.Context, in models.Endpoint) (*models.Endpoint, error) {
	script := fmt.Sprintf("Insert Ignore Into BlaDB.endpoints (path, method, output, status) Values (?, ?, ?, ?)")
	_, err := m.db.ExecContext(ctx, script, in.Path, in.Method, in.Output, in.Status)
	if err != nil {
		log.Println("mysql, New, result, error: ", err)
		return nil, err
	}
	endpoint, err := m.GetByPathAndMethod(ctx, in)
	if err != nil {
		log.Println("mysql, New, item search, error: ", err)
		return nil, err
	}

	return endpoint, nil
}
