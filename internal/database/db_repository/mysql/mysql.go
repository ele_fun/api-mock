package mysql

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/config"
)

type (
	mysql struct {
		db *sql.DB
	}
)

func NewMysql() (*mysql, error) {
	// TODO: dns
	dns := config.Env.DBMysqlDNS
	if dns == "" {
		return nil, fmt.Errorf("dns mysql not found")
	}

	// TODO: connection
	var err error
	db, err := sql.Open("mysql", dns)
	if err != nil {
		log.Println("mysql, connection, error: ", err)
		return nil, err
	}
	// ping
	var isConnected bool
	for i := 0; i < 5; i++ {
		if err = db.Ping(); err == nil {
			isConnected = true
			break
		}
		fmt.Print(" .. ")
		time.Sleep(500 * time.Millisecond)
	}

	if !isConnected {
		log.Println("\nmysql, isNotConnected, error: ", err)
		return nil, err
	}

	return &mysql{
		db: db,
	}, nil
}
