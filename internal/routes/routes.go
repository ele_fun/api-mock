package routes

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type (
	Middleware func(http.HandlerFunc) http.HandlerFunc
	route      struct {
		Methods     []string
		Guards      []Middleware
		HandlerFunc http.HandlerFunc
		Path        string
	}
)

var listRoutes []route

func NewRouter() *mux.Router {
	r := mux.NewRouter()
	listRoutes = append(listRoutes, listRoutesEndpoints()...)

	for _, value := range listRoutes {
		log.Printf("%s  - %s", value.Methods, value.Path)
		r = handlerFunc(r, value)
	}

	r.NotFoundHandler = applyMiddleware(listRoutesMaster()[0])

	return r
}

func handlerFunc(r *mux.Router, value route) *mux.Router {

	for index := len(value.Guards) - 1; index >= 0; index-- {
		log.Println("handlerFunc, index: ", index)
		guard := value.Guards[index]
		value.HandlerFunc = guard(value.HandlerFunc)
	}
	r.HandleFunc(value.Path, value.HandlerFunc).Methods(value.Methods...)
	return r
}

func applyMiddleware(value route) http.HandlerFunc {

	//for index, guard := range value.Guards {
	for index := len(value.Guards) - 1; index >= 0; index-- {
		guard := value.Guards[index]
		log.Println("index: ", index)
		value.HandlerFunc = guard(value.HandlerFunc)
	}
	return value.HandlerFunc
}
