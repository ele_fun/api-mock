package routes

import (
	"fmt"
	"net/http"

	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/handlers/endpoints"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/middlewares"
)

func listRoutesEndpoints() []route {
	h := endpointshandler.Handler
	prefix := "/endpoints"
	list := []route{
		{
			Path:    fmt.Sprintf("%s", prefix),
			Methods: []string{http.MethodPost},
			Guards: []Middleware{
				middlewares.Middleware.Master,
			},
			HandlerFunc: h.New,
		},
	}

	return list
}
