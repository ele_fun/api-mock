package routes

import (
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/handlers/master"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/middlewares"
)

func listRoutesMaster() []route {
	h := master.Handler
	list := []route{
		{
			Guards: []Middleware{
				middlewares.Middleware.Cache,
				middlewares.Middleware.Database,
			},
			HandlerFunc: h.Master,
		},
	}

	return list
}
