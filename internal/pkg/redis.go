package pkg

type (
	keysRedis struct {
		EndpointByPathAndMethod string
	}
)

var KeysRedis keysRedis = keysRedis{}

func init() {
	KeysRedis = keysRedis{
		EndpointByPathAndMethod: "endpoint_path_%s_method_%s",
	}
}
