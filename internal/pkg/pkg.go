package pkg

import "encoding/json"

type (
	Primitive interface {
		uint | uint8 | uint16 | uint32 | uint64 |
			string | int | int8 | int16 | int32
	}
)

func Pointer[T Primitive](t T) *T {
	return &t
}

func ConvertStringToStruct(in string, out any) error {
	return json.Unmarshal([]byte(in), &out)
}
