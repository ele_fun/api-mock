package pkg

import (
	"context"
	"encoding/json"
)

type (
	ContextValue struct {
		Value   string
		Context context.Context
		Out     any
	}
)

func ContextValueToStruct(c ContextValue) error {

	valueContext := c.Context.Value(c.Value)
	nextBytes, err := json.Marshal(valueContext)
	if err != nil {
		return err
	}

	err = json.Unmarshal(nextBytes, &c.Out)
	if err != nil {
		return err
	}

	return nil
}
