package pkg

import "net/http"

type (
	pipes struct {
		Cache string
		DB    string
		Data  string
	}
)

var Pipes pipes = pipes{}

func init() {
	Pipes = pipes{
		Cache: "cache",
		DB:    "database",
		Data:  "data",
	}
}
func MasterPipe(r *http.Request, namePipe string, out any) error {
	err := ContextValueToStruct(ContextValue{
		Value:   namePipe,
		Context: r.Context(),
		Out:     &out,
	})
	return err
}
