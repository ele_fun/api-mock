package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/cache"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/cache/cache_repository/redis"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/config"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/database"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/database/db_repository/mysql"
	"gitlab.com/Elevarup/ele_fun/reverse_proxy/internal/routes"
)

func main() {
	err := config.InitEnv()
	if err != nil {
		os.Exit(0)
	}
	// init db
	dbMysql, err := mysql.NewMysql()
	if err != nil {
		os.Exit(0)
	}
	database.Set(database.Database{EndpointRepository: dbMysql})

	// init cache
	cacheRedis, err := redis.NewRedis()
	if err != nil {
		os.Exit(0)
	}
	cache.Set(cache.CacheStore{Endpoint: cacheRedis})

	// init server http
	fmt.Println("main, api, reverse-proxy")
	srv := http.Server{
		Addr:    fmt.Sprintf("%s:%s", config.Env.AppHost, config.Env.AppPort),
		Handler: routes.NewRouter(),
	}
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Fatalln("liste and server, error: ", err)
			os.Exit(0)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	fmt.Println("shutdown server")
	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()

	srv.Shutdown(ctx)
}
