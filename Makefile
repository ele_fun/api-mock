
all: run

dc:
	docker-compose up -d
down:
	docker-compose down
log:
	docker-compose logs -f
run:
	go run cmd/api/*.go
db:
	docker exec -it db_mysql_rp mysql -u root -ppass_root BlaDB
cache:
	docker exec -it cache_redis_rp redis-cli
monitor:
	docker exec -it cache_redis_rp redis-cli monitor
